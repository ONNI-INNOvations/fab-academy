EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L fab:Conn_01x02_Male J3
U 1 1 60B8EA44
P 6600 4800
F 0 "J3" V 6754 4612 50  0000 R CNN
F 1 "Conn_01x02_Male" V 6663 4612 50  0000 R CNN
F 2 "fab:Header_SMD_01x02_P2.54mm_Horizontal_Male" H 6600 4800 50  0001 C CNN
F 3 "~" H 6600 4800 50  0001 C CNN
	1    6600 4800
	0    -1   -1   0   
$EndComp
$Comp
L fab:Conn_01x04_Male J1
U 1 1 60B8FA4F
P 4100 4900
F 0 "J1" V 4254 4612 50  0000 R CNN
F 1 "Conn_01x04_Male" V 4163 4612 50  0000 R CNN
F 2 "fab:Header_SMD_01x04_P2.54mm_Horizontal_Male" H 4100 4900 50  0001 C CNN
F 3 "~" H 4100 4900 50  0001 C CNN
	1    4100 4900
	0    -1   -1   0   
$EndComp
$Comp
L fab:C C1
U 1 1 60B93D62
P 6750 3450
F 0 "C1" H 6865 3496 50  0000 L CNN
F 1 "C" H 6865 3405 50  0000 L CNN
F 2 "fab:C_1206" H 6788 3300 50  0001 C CNN
F 3 "" H 6750 3450 50  0001 C CNN
	1    6750 3450
	0    -1   -1   0   
$EndComp
$Comp
L fab:MOSFET_N-CH_30V_1.7A Q1
U 1 1 60B9632D
P 4250 2850
F 0 "Q1" H 4358 2892 45  0000 L CNN
F 1 "MOSFET_N-CH_30V_1.7A" H 4358 2808 45  0000 L CNN
F 2 "fab:fab-SOT-23" H 4280 3000 20  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/NDS355AN-D.PDF" H 4250 2850 50  0001 C CNN
	1    4250 2850
	-1   0    0    1   
$EndComp
Text Label 6700 4600 0    50   ~ 0
GND
Text Label 6600 4600 3    50   ~ 0
VCC
Text Label 6800 3950 3    50   ~ 0
GND
Wire Wire Line
	5200 2900 5200 2950
Text Label 3950 4200 3    50   ~ 0
VCC
Wire Wire Line
	6600 3200 6600 3150
Text Label 4000 4700 3    50   ~ 0
PA1
Text Label 4100 4700 0    50   ~ 0
PA7
Text Label 4200 4700 0    50   ~ 0
PA2
Text Label 4300 4700 0    50   ~ 0
PA3
Text Label 5800 3350 0    50   ~ 0
PA1
Text Label 5800 3550 0    50   ~ 0
PA3
Text Label 5800 3750 0    50   ~ 0
PA7
Text Label 5800 3450 0    50   ~ 0
PA2
$Comp
L MCU_Microchip_ATtiny:ATtiny412-SS U1
U 1 1 60B8BDBD
P 5200 3550
F 0 "U1" H 4671 3596 50  0000 R CNN
F 1 "ATtiny412-SS" H 4671 3505 50  0000 R CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5200 3550 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/40001911A.pdf" H 5200 3550 50  0001 C CNN
	1    5200 3550
	1    0    0    -1  
$EndComp
Text Label 5200 4150 3    50   ~ 0
GND
Text Label 5250 4900 0    50   ~ 0
UPDI
Text Label 5150 4900 0    50   ~ 0
GND
$Comp
L fab:Conn_UPDI_01x02_Male J2
U 1 1 60B8CA5E
P 5250 4700
F 0 "J2" H 5262 4925 50  0000 C CNN
F 1 "Conn_UPDI_01x02_Male" H 5262 4834 50  0000 C CNN
F 2 "fab:Header_SMD_UPDI_01x02_P2.54mm_Horizontal_Male" H 5250 4700 50  0001 C CNN
F 3 "~" H 5250 4700 50  0001 C CNN
	1    5250 4700
	0    1    1    0   
$EndComp
Text Label 5800 3250 0    50   ~ 0
UPDI
$Comp
L power:GND #PWR02
U 1 1 60BA535E
P 6700 4650
F 0 "#PWR02" H 6700 4400 50  0001 C CNN
F 1 "GND" H 6705 4477 50  0000 C CNN
F 2 "" H 6700 4650 50  0001 C CNN
F 3 "" H 6700 4650 50  0001 C CNN
	1    6700 4650
	1    0    0    -1  
$EndComp
$Comp
L fab:Regulator_Linear_LM3480-5.0V-100mA U2
U 1 1 60BA627B
P 6850 3950
F 0 "U2" V 6896 4055 50  0000 L CNN
F 1 "Regulator_Linear_LM3480-5.0V-100mA" V 6805 4055 50  0000 L CNN
F 2 "fab:SOT-23" H 6850 4175 50  0001 C CIN
F 3 "https://www.ti.com/lit/ds/symlink/lm3480.pdf" H 6850 3950 50  0001 C CNN
	1    6850 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6850 4250 6600 4250
Text Label 7150 3950 0    50   ~ 0
GND
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 60BAC3E5
P 6600 4250
F 0 "#FLG0101" H 6600 4325 50  0001 C CNN
F 1 "PWR_FLAG" H 6600 4423 50  0000 C CNN
F 2 "" H 6600 4250 50  0001 C CNN
F 3 "~" H 6600 4250 50  0001 C CNN
	1    6600 4250
	1    0    0    -1  
$EndComp
Connection ~ 6600 4250
Wire Wire Line
	6600 4250 6600 4600
$Comp
L fab:LED D1
U 1 1 60BADC4B
P 3950 4050
F 0 "D1" V 3897 4128 50  0000 L CNN
F 1 "LED" V 3988 4128 50  0000 L CNN
F 2 "fab:LED_1206" H 3950 4050 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS-22-98-0002/LTST-C150CKT.pdf" H 3950 4050 50  0001 C CNN
	1    3950 4050
	0    1    1    0   
$EndComp
Text Label 6900 3450 1    50   Italic 10
GND
Text Label 6600 3200 2    50   Italic 10
GND
Text Label 4250 2650 2    50   Italic 10
GND
Text Label 3950 3900 2    50   Italic 10
LEDGND
$Comp
L fab:R R1
U 1 1 60B936A2
P 6600 3000
F 0 "R1" H 6670 3046 50  0000 L CNN
F 1 "R" H 6670 2955 50  0000 L CNN
F 2 "fab:R_1206" V 6530 3000 50  0001 C CNN
F 3 "~" H 6600 3000 50  0001 C CNN
	1    6600 3000
	1    0    0    -1  
$EndComp
Text Label 6600 2850 2    50   Italic 10
PA6
Text Label 5800 3650 2    50   Italic 10
PA6
Text Label 6850 3650 2    50   Italic 10
VO
Text Label 6600 3450 2    50   Italic 10
VO
Text Label 5200 2900 2    50   Italic 10
VO
$Comp
L fab:R R2
U 1 1 60BC9983
P 7250 2700
F 0 "R2" H 7320 2746 50  0000 L CNN
F 1 "R" H 7320 2655 50  0000 L CNN
F 2 "fab:R_1206" V 7180 2700 50  0001 C CNN
F 3 "~" H 7250 2700 50  0001 C CNN
	1    7250 2700
	1    0    0    -1  
$EndComp
$Comp
L fab:LED D2
U 1 1 60BCA11D
P 7800 2900
F 0 "D2" H 7793 3116 50  0000 C CNN
F 1 "LED" H 7793 3025 50  0000 C CNN
F 2 "fab:LED_1206" H 7800 2900 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS-22-98-0002/LTST-C150CKT.pdf" H 7800 2900 50  0001 C CNN
	1    7800 2900
	0    1    1    0   
$EndComp
Text Label 7250 2850 2    50   Italic 10
VO
Text Label 7250 2550 2    50   Italic 10
pwrled
Text Label 7800 3050 2    50   Italic 10
pwrled
Text Label 7800 2750 2    50   Italic 10
GND
Text Label 4250 3050 2    50   Italic 10
PA6
Text Label 4450 2750 2    50   Italic 10
LEDGND
$EndSCHEMATC
