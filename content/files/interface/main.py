import tkinter as tk
from tkinter.ttk import Progressbar
from tkinter import ttk
import time
import serial
from tkinter import *
from tkinter.ttk import *
from random import randint

ser = serial.Serial('COM12', 9800, timeout=None)
parent = tk.Tk()
parent.title("Progressbar")
parent.geometry('700x400')
style = ttk.Style()
style.theme_use('default')
style.configure("black.Horizontal.TProgressbar", background='green')
bar = Progressbar(parent, length=700, style='black.Horizontal.TProgressbar')
bar['value'] = 3
bar.grid(column=0, row=0)
def update():
    line = ser.readline()
    if line:
        string = line.decode().strip()
        num = int(string)
        num2 = int(num%100)
        print(num2)
        bar['value'] = num2
update()
parent.mainloop()
ser.close()
