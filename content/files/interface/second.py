
import serial
import time
import matplotlib.pyplot as plt

ser = serial.Serial('COM12', 9800, timeout=1)
time.sleep(2)

data = []
for i in range(50):
    read = ser.readline()
    if read:
        num = int(read.decode())
        print(num)
        data.append(num)
ser.close()


plt.plot(data)
plt.xlabel('Time')
plt.ylabel('Potentiometer Reading')
plt.title('Potentiometer Reading vs. Time')
plt.show()
