EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L fab:C C1
U 1 1 60B62B63
P 6500 3650
F 0 "C1" H 6615 3696 50  0000 L CNN
F 1 "C" H 6615 3605 50  0000 L CNN
F 2 "fab:C_1206" H 6538 3500 50  0001 C CNN
F 3 "" H 6500 3650 50  0001 C CNN
	1    6500 3650
	1    0    0    -1  
$EndComp
$Comp
L fab:R R1
U 1 1 60B6467B
P 3900 3250
F 0 "R1" H 3970 3296 50  0000 L CNN
F 1 "R" H 3970 3205 50  0000 L CNN
F 2 "fab:R_1206" V 3830 3250 50  0001 C CNN
F 3 "~" H 3900 3250 50  0001 C CNN
	1    3900 3250
	-1   0    0    1   
$EndComp
$Comp
L fab:R R4
U 1 1 60B649E3
P 5200 4250
F 0 "R4" H 5270 4296 50  0000 L CNN
F 1 "R" H 5270 4205 50  0000 L CNN
F 2 "fab:R_1206" V 5130 4250 50  0001 C CNN
F 3 "~" H 5200 4250 50  0001 C CNN
	1    5200 4250
	1    0    0    -1  
$EndComp
$Comp
L fab:R R2
U 1 1 60B6525B
P 4150 3550
F 0 "R2" H 4220 3596 50  0000 L CNN
F 1 "R" H 4220 3505 50  0000 L CNN
F 2 "fab:R_1206" V 4080 3550 50  0001 C CNN
F 3 "~" H 4150 3550 50  0001 C CNN
	1    4150 3550
	1    0    0    -1  
$EndComp
$Comp
L fab:R R3
U 1 1 60B65816
P 4200 4100
F 0 "R3" H 4270 4146 50  0000 L CNN
F 1 "R" H 4270 4055 50  0000 L CNN
F 2 "fab:R_1206" V 4130 4100 50  0001 C CNN
F 3 "~" H 4200 4100 50  0001 C CNN
	1    4200 4100
	1    0    0    -1  
$EndComp
$Comp
L fab:LED D1
U 1 1 60B65F00
P 3800 4100
F 0 "D1" H 3793 4316 50  0000 C CNN
F 1 "LED" H 3793 4225 50  0000 C CNN
F 2 "fab:LED_1206" H 3800 4100 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS-22-98-0002/LTST-C150CKT.pdf" H 3800 4100 50  0001 C CNN
	1    3800 4100
	0    1    1    0   
$EndComp
$Comp
L fab:BUTTON_B3SN SW1
U 1 1 60B66AD6
P 6450 4600
F 0 "SW1" H 6450 4885 50  0000 C CNN
F 1 "BUTTON_B3SN" H 6450 4794 50  0000 C CNN
F 2 "fab:Button_Omron_B3SN_6x6mm" H 6450 4800 50  0001 C CNN
F 3 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-b3sn.pdf" H 6450 4800 50  0001 C CNN
	1    6450 4600
	1    0    0    -1  
$EndComp
$Comp
L fab:Conn_01x06_Male J6
U 1 1 60B6800A
P 7500 3250
F 0 "J6" H 7608 3631 50  0000 C CNN
F 1 "Conn_01x06_Male" H 7608 3540 50  0000 C CNN
F 2 "fab:Header_SMD_01x06_P2.54mm_Horizontal_Male" H 7500 3250 50  0001 C CNN
F 3 "~" H 7500 3250 50  0001 C CNN
	1    7500 3250
	1    0    0    -1  
$EndComp
$Comp
L fab:Conn_01x03_Male J4
U 1 1 60B6900A
P 5450 4550
F 0 "J4" H 5558 4831 50  0000 C CNN
F 1 "Conn_01x03_Male" H 5558 4740 50  0000 C CNN
F 2 "fab:Header_SMD_01x03_P2.54mm_Horizontal_Male" H 5450 4550 50  0001 C CNN
F 3 "~" H 5450 4550 50  0001 C CNN
	1    5450 4550
	0    1    1    0   
$EndComp
$Comp
L fab:Conn_02x03_Male J2
U 1 1 60B6A5D0
P 4800 4450
F 0 "J2" H 4850 4767 50  0000 C CNN
F 1 "Conn_02x03_Male" H 4850 4676 50  0000 C CNN
F 2 "fab:PinHeader_SMD_02x03_P2.54mm_Vertical" H 4800 4450 50  0001 C CNN
F 3 "https://cdn.amphenol-icc.com/media/wysiwyg/files/drawing/95278.pdf" H 4800 4450 50  0001 C CNN
	1    4800 4450
	0    -1   -1   0   
$EndComp
$Comp
L fab:Conn_01x08_Male J3
U 1 1 60B6C098
P 5450 1650
F 0 "J3" V 5285 1578 50  0000 C CNN
F 1 "Conn_01x08_Male" V 5376 1578 50  0000 C CNN
F 2 "fab:Header_SMD_01x08_P2.54mm_Horizontal_Male" H 5450 1650 50  0001 C CNN
F 3 "~" H 5450 1650 50  0001 C CNN
	1    5450 1650
	0    1    1    0   
$EndComp
$Comp
L fab:Conn_01x05_Male J1
U 1 1 60B76693
P 3250 3300
F 0 "J1" H 3222 3232 50  0000 R CNN
F 1 "Conn_01x05_Male" H 3222 3323 50  0000 R CNN
F 2 "fab:Header_SMD_01x05_P2.54mm_Horizontal_Male" H 3250 3300 50  0001 C CNN
F 3 "~" H 3250 3300 50  0001 C CNN
	1    3250 3300
	1    0    0    -1  
$EndComp
$Comp
L fab:Conn_01x02_Male J5
U 1 1 60B79CE8
P 6750 1700
F 0 "J5" V 6812 1744 50  0000 L CNN
F 1 "Conn_01x02_Male" V 6903 1744 50  0000 L CNN
F 2 "fab:Header_SMD_01x02_P2.54mm_Horizontal_Male" H 6750 1700 50  0001 C CNN
F 3 "~" H 6750 1700 50  0001 C CNN
	1    6750 1700
	0    1    1    0   
$EndComp
Text Label 6650 1900 3    50   ~ 0
VD0
Text Label 6750 1900 3    50   ~ 0
GND
Text Label 5650 1850 3    50   ~ 0
GND
Text Label 4550 3200 2    50   ~ 0
VCC
Text Label 6500 3500 0    50   ~ 0
VCC
Text Label 6500 3800 0    50   ~ 0
GND
Text Label 7700 3250 0    50   ~ 0
VCC
Text Label 7700 3050 0    50   ~ 0
GND
NoConn ~ 7700 3150
NoConn ~ 7700 3550
Text Label 7700 3350 0    50   ~ 0
TX
Text Label 7700 3450 0    50   ~ 0
RX
Text Label 4950 2600 1    50   ~ 0
UPDI
Text Label 5050 2600 1    50   ~ 0
PA1
Text Label 5150 2600 1    50   ~ 0
PA2
Text Label 5250 2600 1    50   ~ 0
PA3
Text Label 5350 2600 1    50   ~ 0
PA4
Text Label 5450 2600 1    50   ~ 0
PA5
Text Label 5550 2600 1    50   ~ 0
PA6
Text Label 5650 2600 1    50   ~ 0
PA7
Text Label 5250 3800 3    50   ~ 0
TX
Text Label 5150 3800 3    50   ~ 0
RX
Text Label 5050 3800 3    50   ~ 0
SDA
Text Label 4950 3800 3    50   ~ 0
SCL
$Comp
L fab:Regulator_Linear_NCP1117-5.0V-1A U2
U 1 1 60B6210B
P 6600 2700
F 0 "U2" H 6600 2942 50  0000 C CNN
F 1 "Regulator_Linear_NCP1117-5.0V-1A" H 6600 2851 50  0000 C CNN
F 2 "fab:SOT-223-3_TabPin2" H 6600 2900 50  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/NCP1117-D.PDF" H 6700 2450 50  0001 C CNN
	1    6600 2700
	0    1    1    0   
$EndComp
Text Label 6600 2400 0    50   ~ 0
VCC
Text Label 6600 3000 0    50   ~ 0
VD0
Text Label 6300 2700 0    50   ~ 0
GND
Text Label 5750 1850 3    50   ~ 0
VD0
Text Label 5550 1850 3    50   ~ 0
VCC
Text Label 5450 1850 3    50   ~ 0
PA4
Text Label 5350 1850 3    50   ~ 0
PA5
Text Label 5250 1850 3    50   ~ 0
PA6
Text Label 5150 1850 3    50   ~ 0
PA7
Text Label 5050 1850 3    50   ~ 0
GND
Text Label 3450 3100 0    50   ~ 0
GND
Text Label 3450 3200 0    50   ~ 0
VCC
Text Label 3450 3300 0    50   ~ 0
SDA
Text Label 3450 3400 0    50   ~ 0
SCL
Text Label 3450 3500 0    50   ~ 0
GND
Text Label 6650 4600 0    50   ~ 0
VCC
Text Label 6250 4600 2    50   ~ 0
PA3
Text Label 4900 4150 1    50   ~ 0
PA3
Text Label 4800 4150 1    50   ~ 0
PA2
Text Label 4700 4150 1    50   ~ 0
PA1
Text Label 4700 4650 3    50   ~ 0
GND
Text Label 4800 4650 3    50   ~ 0
GND
Text Label 4900 4650 3    50   ~ 0
GND
Text Label 5350 4750 3    50   ~ 0
VCC
Text Label 5450 4750 3    50   ~ 0
GND
Text Label 5550 4750 3    50   ~ 0
UPDI
Text Label 5200 4400 0    50   ~ 0
GND
Text Label 5200 4100 0    50   ~ 0
PA3
Text Label 3800 3950 0    50   ~ 0
GND
Wire Wire Line
	3800 4250 4200 4250
Text Label 4200 3950 0    50   ~ 0
PA1
Text Label 3900 3100 0    50   ~ 0
VCC
Text Label 4150 3400 0    50   ~ 0
VCC
Text Label 3900 3400 0    50   ~ 0
SDA
$Comp
L fab:Microcontroller_ATtiny1614-SSFR U1
U 1 1 60B610E3
P 5250 3200
F 0 "U1" H 5250 4081 50  0000 C CNN
F 1 "Microcontroller_ATtiny1614-SSFR" H 5250 3990 50  0000 C CNN
F 2 "fab:SOIC-14_3.9x8.7mm_P1.27mm" H 5250 3200 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf" H 5250 3200 50  0001 C CNN
	1    5250 3200
	0    -1   -1   0   
$EndComp
Text Label 5950 3200 0    50   ~ 0
GND
Text Label 4150 3700 0    50   ~ 0
SCL
$Comp
L Power:PWR_FLAG #FLG0101
U 1 1 60B98334
P 4550 3200
F 0 "#FLG0101" H 4550 3275 50  0001 C CNN
F 1 "PWR_FLAG" H 4550 3373 50  0000 C CNN
F 2 "" H 4550 3200 50  0001 C CNN
F 3 "~" H 4550 3200 50  0001 C CNN
	1    4550 3200
	1    0    0    -1  
$EndComp
$Comp
L Power:GND #PWR0102
U 1 1 60B98CA3
P 6300 2700
F 0 "#PWR0102" H 6300 2450 50  0001 C CNN
F 1 "GND" H 6305 2527 50  0000 C CNN
F 2 "" H 6300 2700 50  0001 C CNN
F 3 "" H 6300 2700 50  0001 C CNN
	1    6300 2700
	1    0    0    -1  
$EndComp
$Comp
L Power:PWR_FLAG #FLG0102
U 1 1 60B9976F
P 5950 3200
F 0 "#FLG0102" H 5950 3275 50  0001 C CNN
F 1 "PWR_FLAG" H 5950 3373 50  0000 C CNN
F 2 "" H 5950 3200 50  0001 C CNN
F 3 "~" H 5950 3200 50  0001 C CNN
	1    5950 3200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
