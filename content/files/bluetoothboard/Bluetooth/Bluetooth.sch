EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RF_Bluetooth:RN4871 U1
U 1 1 60BF2FDA
P 4450 3350
F 0 "U1" H 4450 4131 50  0000 C CNN
F 1 "RN4871" H 4450 4040 50  0000 C CNN
F 2 "RF_Module:Microchip_RN4871" H 4450 2650 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/50002489A.pdf" H 3950 3900 50  0001 C CNN
	1    4450 3350
	1    0    0    -1  
$EndComp
$Comp
L fab:R R1
U 1 1 60BF3E26
P 3100 3000
F 0 "R1" H 3170 3046 50  0000 L CNN
F 1 "R" H 3170 2955 50  0000 L CNN
F 2 "fab:R_1206" V 3030 3000 50  0001 C CNN
F 3 "~" H 3100 3000 50  0001 C CNN
	1    3100 3000
	1    0    0    -1  
$EndComp
$Comp
L fab:C C1
U 1 1 60BF412A
P 7600 3150
F 0 "C1" H 7715 3196 50  0000 L CNN
F 1 "C" H 7715 3105 50  0000 L CNN
F 2 "fab:C_1206" H 7638 3000 50  0001 C CNN
F 3 "" H 7600 3150 50  0001 C CNN
	1    7600 3150
	1    0    0    -1  
$EndComp
$Comp
L fab:Regulator_Linear_LM3480-3.3V-100mA U2
U 1 1 60BF479B
P 6050 2000
F 0 "U2" H 6050 2242 50  0000 C CNN
F 1 "Regulator_Linear_LM3480-3.3V-100mA" H 6050 2151 50  0000 C CNN
F 2 "fab:SOT-23" H 6050 2225 50  0001 C CIN
F 3 "https://www.ti.com/lit/ds/symlink/lm3480.pdf" H 6050 2000 50  0001 C CNN
	1    6050 2000
	1    0    0    -1  
$EndComp
Text Label 3750 3450 0    50   ~ 0
RST
Text Label 3100 3150 0    50   ~ 0
RST
Text Label 3100 2850 0    50   ~ 0
VO
Text Label 6350 2000 0    50   ~ 0
VO
Text Label 7600 3300 0    50   ~ 0
VO
Text Label 7600 3000 0    50   ~ 0
GND
Text Label 6050 2300 0    50   ~ 0
GND
Text Label 4350 3950 0    50   ~ 0
GND
Text Label 4550 3950 0    50   ~ 0
GND
Text Label 5150 3150 0    50   ~ 0
RIN
Text Label 4450 2750 0    50   ~ 0
VO
$Comp
L fab:Conn_FTDI_01x06_Male J1
U 1 1 60BF7821
P 6350 3600
F 0 "J1" H 6387 4025 50  0000 C CNN
F 1 "Conn_FTDI_01x06_Male" H 6387 3934 50  0000 C CNN
F 2 "fab:Header_SMD_FTDI_01x06_P2.54mm_Horizontal_Male" H 6350 3600 50  0001 C CNN
F 3 "~" H 6350 3600 50  0001 C CNN
	1    6350 3600
	1    0    0    -1  
$EndComp
Text Label 6550 3400 0    50   ~ 0
GND
Text Label 5750 2000 0    50   ~ 0
VI
Text Label 6550 3600 0    50   ~ 0
VI
Text Label 6550 3700 0    50   ~ 0
RX
Text Label 3750 3050 0    50   ~ 0
RX
Text Label 3750 3150 0    50   ~ 0
TX
Text Label 6550 3800 0    50   ~ 0
TX
NoConn ~ 6550 3500
NoConn ~ 6550 3900
NoConn ~ 3750 3250
NoConn ~ 3750 3550
NoConn ~ 3750 3650
NoConn ~ 5150 3650
NoConn ~ 5150 3550
NoConn ~ 5150 3450
NoConn ~ 5150 3350
NoConn ~ 5150 3250
NoConn ~ 5150 3050
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 60BF8997
P 6550 3600
F 0 "#FLG0101" H 6550 3675 50  0001 C CNN
F 1 "PWR_FLAG" H 6550 3773 50  0000 C CNN
F 2 "" H 6550 3600 50  0001 C CNN
F 3 "~" H 6550 3600 50  0001 C CNN
	1    6550 3600
	1    0    0    -1  
$EndComp
$Comp
L fab:BUTTON_B3SN SW?
U 1 1 60BFB884
P 7300 2450
F 0 "SW?" H 7300 2735 50  0000 C CNN
F 1 "BUTTON_B3SN" H 7300 2644 50  0000 C CNN
F 2 "fab:Button_Omron_B3SN_6x6mm" H 7300 2650 50  0001 C CNN
F 3 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-b3sn.pdf" H 7300 2650 50  0001 C CNN
	1    7300 2450
	1    0    0    -1  
$EndComp
Text Label 7100 2450 0    50   ~ 0
RIN
Text Label 7500 2450 0    50   ~ 0
GND
$EndSCHEMATC
