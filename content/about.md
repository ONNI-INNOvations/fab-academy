+++
title = "About Page"
description ="Here I will tell you about me"
+++


![Me repairing a printer](../media/printteri.jpg)

I'm a first year engineering student at Aalto-university and I'm currently studying automation and information technology.

I have always been interested in projects and electronics. My first projects included small mechanical projects that I build with my dad, but later I became much more interested in electronics.
 One of the first electronics projects I made was an arduino powered automatic airsoft turret, it took a long time but it was fun and really great experience.
About 4 years ago I got really interested in 3D printing also, as the idea of having something that you designed in pc become reality in front of your eyes was so exiting.
Since then I have been 3D-printing and modeling quite often and bought a few more printers. I even started a small business selling my 3D-printing services and 3-D prints 3 years ago and I am still doing it today.

I was really excited for the Fablab course, because I was a part of Hacklab when I was younger and I loved the culture and everything about it. Fablab seems to be pretty similiar to Hacklab actually!
 I have also always wanted to learn more about design, CNC-machining, casting and to meet more likeminded people. That's why I think the Fablab academy courses will be very rewarding and I can't wait to learn more!
