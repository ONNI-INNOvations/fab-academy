+++
title = "My tenth assignment"
description ="Machine Building"
+++

## Group assignment

Our idea was to make a punching machine.

Here is a link to the group assignment, still needs to be polished.

https://gitlab.com/aaltofablab/machine-building-2021-group-b

## Individual assignment

I had some trouble at first in thinking how I will design and produce the moving and mechanical parts of the punching machine.
I thought about it over the weekend and got some inspiration from pistons, and made some research on wikipedia : https://en.wikipedia.org/wiki/Piston

I found two rods in the lab with linear bearings and also some rods without bearings and ball bearings.

For this project 2 rods with linear bearings, one rod without bearings and 2 ball bearings are needed.

I didn't remember to take pictures while designing the part so I'll try my best to explain it.

Here are the all the moving parts together. The idea is for the motor to spin the round part which will move the other parts. The part on the lower left with two holes will, have the linear bearings and rods inside to keep it steady and straight. The other rod will be in the turning parts on top to change the circular motion into linear motion.

![mach-1](../../media/machine/machine-01.png)

Here are all the parameters I used for this build

![mach-2](../../media/machine/machine-02.png)

A closer look on the moving parts on top where the one rod will be located, these parts will have bearings inside them to allow it to turn.

![mach-3](../../media/machine/machine-03.png)

Here are all the parts with the motor side of the machine, including motor/ linear bearing rod holders and the spinning circle with the moving top parts.

![mach-4](../../media/machine/machine-04.png)

Here is how the spinning circle looks on top and how it works, it has small "rods" coming out of it to allow for ball bearing attachment.

![mach-6](../../media/machine/machine-06.png)

Overall this week was really fun I, even though I'm very busy with other stuff as well. I learned to make a machine which turns circular motion into linear motion and also got to play around more with parameters in fusion and bearings in general! A great week and it was nice that there was some extra time this week so I had some time to catch up.


Here's the fusion design files and the stl:s

{{< rawhtml >}}
  <p class="speshal-fancy-custom">
  <a href="../../files/Machine-updated.zip" download >Machine design files</a>
  </p>
{{< /rawhtml >}}
