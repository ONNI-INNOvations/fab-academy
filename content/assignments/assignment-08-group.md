+++
title = "My eight assignment - Group"
description ="Computer-Controller Machining"
+++

Group members : Dinan, Onni, Russian

## Group assignment

This week, we explored the Recontech 1312 CNC milling machine. We used an 8mm flat endmill tool and corresponding shaft.

![G-WOOD-1](../../media/groupwood/groupwood-01.jpg)


To prevent the stock moving during the milling process, we utilized the vacuum feature. We opened holes and placed rubber bands according to the size and shape of the stock. And to protect the machine itself, we placed a sacrificial layer under the stock.

![G-WOOD-2](../../media/groupwood/groupwood-02.jpg)

We added tabs in design to secure the parts. To remove parts after milling, we used a hammer and a chisel.

![G-WOOD-9](../../media/groupwood/groupwood-09.jpg)

Here is a close-up of tabs.

![G-WOOD-3](../../media/groupwood/groupwood-03.jpg)

## Conventional vs climb

We got testing pieces to compare the difference between conventional and climb milling.

Conventional milling: flutes rotate against the feed (direction of cutting)
Climb milling: flutes rotate with the feed

Usually climb milling gives longer tool life and better surface finnish because of lower cutting forces. There is less recutting since chips fall behind the cutter. However, the cutter can suck the workpiece into the cut and break the tool with climb milling if the machine has backlash.

When cutting up to half the cutter’s diameter, climb milling is a better choice. When cutting more than ¾ of the diameter, conventional milling performs better.

Here is a nice video introducing those two: https://youtu.be/aAhyidrMD2I

![G-WOOD-4](../../media/groupwood/groupwood-04.jpg)

We measured the width of both pieces and there were no big difference on outcomes.

![G-WOOD-5](../../media/groupwood/groupwood-05.jpg)
![G-WOOD-6](../../media/groupwood/groupwood-06.jpg)

![G-WOOD-7](../../media/groupwood/groupwood-07.jpg)
![G-WOOD-7](../../media/groupwood/groupwood-07.jpg)
