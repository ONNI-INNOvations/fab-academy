+++
title = "My fifth assignment"
description ="Electronics Production"
+++

This week we learned how to use CNC-machines for milling out our custom circuit boards and how to solder components to them to make a functional in-circuit programmer.

## CNC-machining group

Here is the link for the group assignment https://eeropic.gitlab.io/fablab-doc/assignments/assignment-05-group/

## CNC-machining individual

In the introduction Kris showed us how to use the CNC-machine and milled a small example to show the capabilities of the CNC-machine. At first he cut a small piece with 0.4mm bit to show how precise the machine can make and how the different values impact the final result.

![CNC-01](../../media/cnc/cnc-01.jpg)

Here we can see that with the current number of offsets and a 0.4mm bit the middle came out pretty good, but it started having problems when the gaps became too small they were not noticable anymore. We learned that the recommended speeds with the different bits were 1mm/s with 0.3mm bit 1.5mm/s with 0.4mm and 2mm/s with 0.8mm. The 0.3 and 0.4 bits were useful when milling the copper to make the traces for the pcb and the 0.8mm worked well when cutting the pcb outline. The copper's thickness was 0.1mm so when we were cutting the traces we wanted to use 0.1mm depth to cut and when we were cutting the part out with 0.8mm bit we wanted to cut in increments of 0.6mm, and the totaland max depth was 1.8mm for the 2mm pcb we used.

So next I started to make my own pcb, I started by placing a scrap pcb on the cnc bed with double sided tape

![CNC-02](../../media/cnc/cnc-02.jpg)

Next I put the 0.3mm bit in for doing the traces, and used the wrenches to tighten it up so it wouldn't fall.

![CNC-03](../../media/cnc/cnc-03.jpg)

Then I navigated to the fablab page and from there to the current assignment and found the png files to make the traces and outline to cut the pcb.

![CNC-04](../../media/cnc/cnc-04.jpg)

Next I moved the bit to the lower left corner of the pcb using the controls in VPanel and then I set the XY origin there.

![CNC-05](../../media/cnc/cnc-05.jpg)

Then I placed the sensing block with a known height of 15mm on the pcb and used the set Z origin using sensor attribute to set the Z origin.

![CNC-06](../../media/cnc/cnc-06.jpg)

Here are the settings I used for the traces, the image is a little blurry but you should see everything. First I uploaded the png and added the save file attribute at the end. Then I chose the mill traces and changed to the correct tool diameter. I set the cut and max depth at 0.1mm to cut the cupper and used a offset number of 6. I think 5-6 is good for 0.3mm bit and 4 is better for 0.4mm bit. I then set the speed to 1mm/s and the origins to 0. I kept the jog height at 2 and changed the home to 0,0,5.

![CNC-07](../../media/cnc/cnc-07.jpg)

Then right after that I decided to make the same thing for the outline. I just changed the some of the settings. I chose mill outline, changed tool diameter to 0.8mm, cut depth to 0.6mm and max depth 1.8mm so it will cut the pcb in three increments. I also changed the offset number to 1 so it wouldn't cut it as fast as possible. I also changed the speed to 2mm/s but kept the other values the same.

![CNC-08](../../media/cnc/cnc-08.jpg)

I had to cut it again cause my scrap pcb was not even, next I picked up a fresh clean pcb and now when I cut it everything worked perfectly. Here is how it looked after both of the cuts.

![CNC-09](../../media/cnc/cnc-09.jpg)

Next I removed the part and cleaned the cnc machine. Then I used the same double-sided tape and placed the pcb on top of the microscope.

![CNC-10](../../media/cnc/cnc-10.jpg)

Then I applied flux and placed the hardest part(the brainds) in it's right spot. This is how it looked through the camera connected to the microscope.

![CNC-11](../../media/cnc/cnc-11.jpg)

I forgot to take more images of the soldering, but I had some problems with the biggest part as I sometimes got some tin in between the small legs and it was really hard to get out, thinking back I should have just changed the chip but I was too determined. Eventually I used some sharp utility knife to move and cut some of the solder with it to get the legs separately.

Then I soldered rest of the components in there and they were quite easy to solder into the pcb. I was in a hurry to get to eat so I soldered rest of the stuff really fast, certainly not my best soldering work. It's not pretty, but it should work.  

![CNC-12](../../media/cnc/cnc-12.jpg)

Later on I noticed that some of the soldering was bad, but with the help of Kris we got it fixed and working!
Here it shows in windows 10 devices when connected, so everything works!

![CNC](../../media/cnc/cnc.png)

Overall I think this week was really rewarding and interesting, I learned how to properly use CNC-machines and some important information about them. I also learned how easy it would be to make your whole own board from scratch, that would be cool to do in the final project!
Solomon tested the part and it works well!
