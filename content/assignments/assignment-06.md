+++
title = "My sixth assignment"
description ="3D Scanning and Printing"
+++

I have used 3D printers for multiple years and own a few so this week was pretty easy for me, but it was still interesting. I learned more about different machines and 3mm filaments. I also learned some interesting history behind the 3D-printers.

## 3D-Printing group assignment

Here is a link to the 3D-printing assignment: https://eeropic.gitlab.io/fablab-doc/assignments/assignment-06-group/

## 3D-Printing

So I thought about something that could only be done with additive manufacturing so I decided to make a "bird cage" with a ball inside. You can't take the ball outside and the part is manufactured with the moving ball inside already.

First I made a circle and extruded it to 2mm thickness

![pr-01](../../media/printing/printing-01.png)

Next I created a second circle inside it, extruded it and filleted the corners to make it look nicer.

![pr-02](../../media/printing/printing-02.png)

Then I created a pillar, extruded it and used the circle pattern tool to make 9 identical ones.

![pr-03](../../media/printing/printing-03.png)

Then I created a place on the halfway on the pillars and used a mirror tool to mirror the base

![pr-04](../../media/printing/printing-04.png)

Then I created a sphere using the sphere tool and moved it inside the cage just so it is slightly touching the base. Then I combined all the parts into one.

![pr-05](../../media/printing/printing-05.png)

Lastly after slicing it for the first time I realised the overhangs are gonna be a problem, that's why I filleted the pillar bases to make it look nicer and be easier to print.

This is how it looked finally in the slicer:

![pr-06](../../media/printing/printing-06.png)

This is how it looked after printing with a prusa mk3s. It works well and overall I'm happy with the results.

![pr-07](../../media/printing/printing-07.jpg)

## 3D-Scanning

I hadn't yet properly 3D scanned before so now that I got to do it, it was really interesting and fun. I decided to scan my friend Daniel. So I decided to use the software skanect and an asus scanner that uses IR light to capture the depth in the images and eventually develop the model.
So Daniel sat down on the spinning chair and we started Scanning

![scn-01](../../media/printing/scanning-01.jpg)

This is how it looked during the scanning, Daniel was also spinning around slowly the whole time to get all the areas

![scn-02](../../media/printing/scanning-02.jpg)

Now the scan was done and this is how it looked

![scn-03](../../media/printing/scanning-03.jpg)

Next we needed to fix the scan a little bit so we went to the process tab and chose move and crop and cropped the non essential parts.

![scn-04](../../media/printing/scanning-04.jpg)

Lastly we used the other process stuff as well and made it watertight and simplified the mesh.

![scn-05](../../media/printing/scanning-05.jpg)

Now it was ready and I downloaded it as an stl, essentially it was now ready for printing. Just need to load it into a slicer, slice it into gcode and start printing!

## Here are all the files that I made during this exercise.

{{< rawhtml >}}
  <p class="speshal-fancy-custom">
  <a href="../../files/daniel.stl" download >3D scan</a>
  <a href="../../files/ballcage.stl" download >3D model ready to print</a>
  <a href="../../files/Ball cage.obj" download >3D model cad file</a>
  </p>
{{< /rawhtml >}}
