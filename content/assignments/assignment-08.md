+++
title = "My eight assignment"
description ="Computer-Controller Machining"
+++

This week our goal was to create something big using plywood, a big cnc machine and design software. This was a great week because I learned more about what softwares to use and how to use them. It was also great to learn more about how to use large machines safely and with great care.

## Group assignment

https://onni-innovations.gitlab.io/fab-academy/assignments/assignment-08-group/

## Individual assignment

So first up I needed to design the model. For this I decided to use Fusion 360 as it was already a little more familiar to me and it had some easy to use parametric features to allow me to easily change parameters later on.

My idea was to make a an extra stand for your monitor and other stuff that you can place on top of your normal desk. This allows you to make any of your tables into a standing table.

So I got to designing it, here is the first sketch I made of the side of the table. All the dimensions are in parameters so it's easy to change them later on.

![WOOD-1](../../media/wood/wood-01.png)

Next on I extruded it to the thickness parameter and then added basic monitor holders and front desks. I also used fillet to make all the corners more pretty.

![WOOD-2](../../media/wood/wood-02.png)

I noticed the design really needed a backplate in order to make the design more stable so I designed the holes for it and put on the backplate.

![WOOD-3](../../media/wood/wood-03.png)

![WOOD-4](../../media/wood/wood-04.png)

I decided to make the toolpaths in Vcarve as that was more familiar to me than Fusion 360s toolpath generation so I needed to get the files out as .dfx files for the Vcarve software on the cnc machine. Now thinking later on I could have used Fusion to make the toolpaths as easily and It would have been a good experience, but I'll do it in the next cutting!
Next on I wanted the files to be .dfx for the cnc machine software so I chose a face of the files and pressed p to project the face. Then I chose the projected sketch and saved it as .dfx

![WOOD-5](../../media/wood/wood-05.png)

Now I managed to upload the dfx files on to the cnc computer and this is how they looked on the 120x120x17.7 base.

![WOOD-6](../../media/wood/wood-06.jpg)

Next I chose add outline and chose the vector of the first side in order to only cut it first in order to make sure everything is okay. For the outline I wanted it to cut outside the line and I added some tabs to make it stay in place when cutting the piece off.

![WOOD-7](../../media/wood/wood-07.jpg)

Next I started to setup the tool, I put the 8mm flatend tool to the cnc machine and used an online plunge rate calculator to calculate what to use.
I calculated the feedrate with the equation RPM x CHIP LOAD x Number of teeth. I used 18000 RPM as that was the recommended RPM for the machine.
I checked the chip load from the instructions and got around 0.25 for the size plywood I had. The number of teeth/flutes on the 8mm bit were 2, 18000 *0.25 * 2= 9000 so I got the 9000 feedrate.
I decided to use 18000 in the spindle speed and with 2 cutting ends in the bit I got the 9000 feedrate and 1200 plunge rate. They seemed to work fine.

![WOOD-8](../../media/wood/wood-08.jpg)

Then I got on adding some more passes on the cut in order to try to make the face of it a little more pretty, next I will probably use downcut bits to try to get the prettier outcome as it seems the upcut bit was the biggest problem.

![WOOD-9](../../media/wood/wood-09.jpg)

Lastly before cutting I added some dog bone fillets on all the press fit parts in the design to allow press fits with no problem.

![WOOD-10](../../media/wood/wood-10.jpg)

Now that everything was ready I just calculated the parts and made a toolpath gcode file out of them, now the files were ready.

Then I just put on the vacuum, calibrated the z-axis and got to cutting. It went pretty well, but one of the parts was too high up and hit limit switch. I moved the parts a little more down and everything was good then.
Then I took the parts out of the cnc, cleaned up, took out the bit and shut down the machine.
I was in quite a hurry but I decided to still clean it up in the woodworking department, I managed to do it and then I put it together.

So this is how it finally looked like

![WOOD-11](../../media/wood/wood-11.jpg)

![WOOD-12](../../media/wood/wood-12.jpg)

This week was great because I learned so much more about CNC-machines and designing parts to be cut with them. I definitely want to do this again with a proper hardwood material and make the wood look really nice.
Overall a great week and learning experience, I will definitely use the cnc in the future as well.

Here you can download the dfx and svg files of the design.

{{< rawhtml >}}
  <p class="speshal-fancy-custom">
  <a href="../../files/cnc desk.zip" download >CNC design</a>
  </p>
{{< /rawhtml >}}
