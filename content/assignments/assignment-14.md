+++
title = "My Fourteenth assignment"
description ="Networking and Communications"
+++

For the networking and communications week I wanted to use a bluetooth module to communicate with my computer. For this assignment I chose the module rn4871 because we had them at the lab and form it's datasheet it seemed good enough for this purpose.
I started by making the schematic for the board and adding all the necessary components.

![NET-01](../../media/networking/networking-01.png)

Next I moved it to circuit planner, connected the components and put a ground fill on the board, I also added a margin around the board.

![NET-02](../../media/networking/networking-02.png)

Then I cut it out, soldered it and got it to look pretty nice already on the first try!

![NET-03](../../media/networking/bt-01.jpg)

Next up I installed BLE scanner on my phone to connect and interact with the chip. Then I connected the chip with ftdi to my computer.
I found it as RN 4870-0000 on the phone and connected to it.

![NET-04](../../media/networking/bt-02.jpg)

I also had opened usb serial in arduino to show what's happening in the module. as I connected with my phone it was shown in the arduino serial monitor.

![NET-05](../../media/networking/bt-03.jpg)

Next I decided to write some value to the board from my phone to see if the communication worked that way.

![NET-06](../../media/networking/bt-04.jpg)

After sending it we can see in the serial monitor that the chip received the message

![NET-07](../../media/networking/bt-05.jpg)

Next I wrote a message on the computer "hello to you" to send through the bluetooth connection to the phone

![NET-08](../../media/networking/bt-06.jpg)

Now we can see here that the message was succesfully sent and received on the phone so the communication that way worked as well.
The board has a button on it and it can also be programmed to send messages or react to pressing the button differently.
I also tried connecting this board to an adriardino and have it communicate with it and it seemed to work well! Didn't remember to take any pictures of it though.


Here's the  design files

{{< rawhtml >}}
  <p class="speshal-fancy-custom">
  <a href="../../files/bluetoothboard.zip" download >bluetoothboard design files</a>
  </p>
{{< /rawhtml >}}
