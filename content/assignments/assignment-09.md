+++
title = "My ninth assignment"
description ="Embedded programming"
+++

## Different microcontrollers

The are so many different microcontrollers to use in electronics, and usually in commercial applications they can be even custom made. In prototyping and in development usually ready made boards are used. The one I'm most familiar with is arduino, which has a ton of different variations for example arduino uno and arduino nano. These can be programmed using the arduino programming language and IDE. They are useful for many basic applications and can be even built to very complex projects.

Then also there is raspberry pi which is more of a small computer but they still have the ability to work as a microcontroller as well thanks to the pins they offer. It has the ability to work as a computer and control some basic components, not as much room as in the arduino and it's pricier as well. Can be used together with arduino as well for complicated projects.

Then there are so many more as well but interesting wireless microcontrollers are esp-8266 and esp-32. These are useful wireless microcontrollers that can be connected to a wifi or to the internet, allowing remote use and access. Can be used to build IoT devices. I have used the esp-8266 before but interested in learning esp-32 as well.
These can usually be coded with arduino, but I have liked to flash micropython on them because I think it's better and I'm more familiar with the python coding language.


## Individual assignment

I decided to read the attiny412 datasheet as I have used it twice now and I really like it's abilities and simplicity.
I found the datasheet on digikey : https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny212-412-DataSheet-DS40001911B.pdf
From the datasheet I already knew the basic layout but it was useful to see more stuff about it :

The ATtiny412 is amember of the tinyAVR® 1-series of microcontrollers, using the AVR® processor
with hardware multiplier, running at up to 20 MHz, with 4 KB Flash, 256 bytes of SRAM, and 128
bytes of EEPROM in an 8-pin package.

![data-1](../../media/embedded/datasheet_01.png)

In the datasheet it seems that we should use 4.5 to 5.6vcc voltage to power the board.

 VDD is the external voltage applied to the board

 There is also One USART with fractional baud rate generator, auto-baud, and start-of-frame detection
 It is a serial communication module. The USART supports full duplex communication, asynchronous and synchronous operation, and one-wire configurations.
 The USART can be used for SPI communication
 The USART has 4 parts:
 RxD and Txd for receiving and trasmitting
 XCK and SPI for the clock and a high-speed data transfer interface.

There is also an external clock, it is taken directly from the pin.
This pin is automatically configured for external clock if it's requested.
The pin is a GPIO pin which means General Purpose Input or Output


![emb_2](../../media/embedded/e_2.png)

programming

For the programming I wanted to use arduino with SpenceKondes megaTinyCore library. https://github.com/SpenceKonde/megaTinyCore/blob/master/Installation.md
I used their instructions to add the library as board manager to arduino.

![emb_1](../../media/embedded/e_1.png)

Next I downloaded pyupdi to enable updi programming. I already had python in my computer so I downloaded it using pip from github. https://github.com/mraardvark/pyupdi

I needed to find the preferences.txt file in arduino IDE so I opened file preferences and there I could find the preferences.txt file

![emb_3](../../media/embedded/e_3.png)

build.path=C:\Users\Onni\Documents\Arduino\build
to arduino preferences.txt file.

Now when I build the code using arduino IDE I get the build files in C:\Users\Onni\Documents\Arduino\build. There I could find the hex code that I want to upload to the board using pyupdi.

I wanted to test te board with neils echo code.
http://academy.cba.mit.edu/classes/embedded_programming/t412/hello.t412.echo.ino

I ran it in arduino IDE and the build files updated.

Finally I ran the code  pyupdi -d tiny412 -b 19200 -c COM7 -f C:\Users\Onni\Documents\Arduino\build\Arduino_p_.ino.hex -v in cmder and started uploading through updi.

The programming took a minute but in the end it worked!

![emb_5](../../media/embedded/e_5.png)

Next I created my own code to turn the led on and off when the button is pressed and it works
```
#define LED 4
#define BUTTON 3

void setup() {
  pinMode(LED, OUTPUT);
  pinMode(BUTTON, INPUT_PULLUP);

  Serial.begin(115200);
  digitalWrite(LED, LOW);
}

void loop() {
  int x=!digitalRead(BUTTON);

  if(x){
    digitalWrite(LED, HIGH);
  }else{
    digitalWrite(LED, LOW);
  }

  delay(100);
}
```
It works now !!

Here's a video of it :
{{< rawhtml >}}
<video width="320" height="240" controls>
  <source src="../../media/embedded/embedded.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
{{< /rawhtml >}}
