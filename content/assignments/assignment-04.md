+++
title = "My fourth assignment"
description ="Computer controlled cutting"
+++

This week we made different stuff utilizing a vinyl cutter, heat press and a laser cutter.

Here's a link to our group assignment: https://eeropic.gitlab.io/fablab-doc/assignments/assignment-04-group/

## Vinyl Cutting and Heatpress

I had this great idea to put a mystery QR code on my t-shirt to make people want to scan it when I'm with friends or in public. I used a QR code generator to make a link to a great video, downloaded it as an SVG and opened it in illustrator. I opened Roland Cutstudio in it, flipped the QR code and cut it with the vinyl cutter using the strength we got from tests which was 170.

![Vinyl-01](../../media/vinyl-01.jpg)

Next up I removed the excess using some tweezers and used a pic of the QR code as help.

![Vinyl-02](../../media/vinyl-02.jpg)

Then I pressed the cut vinyl on the heat press for 12 seconds, and let it cool down. Next I took it out and started removing the protective plastic film.

![Vinyl-03](../../media/vinyl-03.jpg)

This was the final result!

![Vinyl-04](../../media/vinyl-04.jpg)

It seems to stay on well, and I will definitely be using it in the future. I can't even think about all the great stuff I can make with this.

## Laser Cutting

I learned how to laser cut in the group and it was a great experience, you can find a link to the group assignment at the start of this page.

The main thing was that we calculated in the group assignment is the kerf and got it to be around 0.13mm for 2mm plywood.
At first I had no idea what to do and started making some basic pieces for the press fit kit.

![Laser-01](../../media/laser/laser-01.png)

Next I used the pattern function in fusion to copy it around and make different variations of it.

![Laser-02](../../media/laser/laser-02.png)

The next day I got the idea what I want to do and I started designing a modular rubber band racing car, which can also be turned into a toy plane.
First I started making the body

![Laser-03](../../media/laser/laser-03.png)

Then I designed the wheels and used circular pattern to make holes in the wheels.

![Laser-04](../../media/laser/laser-04.png)

Then these were ready, I also mirrored the body to help me vizualise the project.

![Laser-05](../../media/laser/laser-05.png)

Using the same workflow I created new parameters for all the new different parts and made the rest

![Laser-06](../../media/laser/laser-06.png)

These were the final parameters and parts!

![Laser-07](../../media/laser/laser-07.png)

After that I cut the parts using the default settings for 2mm plywood in the laser cutter. It all went smoothly. This is how it looked partly assembled.

![Laser-08](../../media/laser/laser-08.jpg)

You can add pretty much anything to this using the rectangular parts I designed first, and you can assemble the car/plane in multiple ways.
I made kind of a mix for demonstrative purposes.

![Laser-09](../../media/laser/laser-09.jpg)

This is the front view.

![Laser-10](../../media/laser/laser-10.jpg)

Lastly I will show how to wind the machine/ rubber bands.

![Laser-11](../../media/laser/laser-11.jpg)

You insert the rubber band in the places in the pulley and start turning it.

![Laser-12](../../media/laser/laser-12.jpg)

When that's done you can release it and the car will move forwards quite fast!
The whole model is made parametrically so they can easily be changed to make the product in thicker or slimmer materials and to use different sizes and other parameters. The idea would be to use a proper wooden rod as a shaft, but I didn't have that available and the rectangular sticks aren't as good. They worked ok though.
Overall I think this week was really great, I learned so much in the group assignment and especially from designing my own press fit kit. I will definitely be using a laser cutter in the future as well.

Here are the design files:

{{< rawhtml >}}
  <p class="speshal-fancy-custom">
  <a href="../../files/press.stl" download >All press fit kit parts as an stl</a>
  <a href="../../files/press.obj" download >All press fit kit parts as an obj</a>
  </p>
{{< /rawhtml >}}
