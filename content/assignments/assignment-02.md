+++
title = "My second assignment"
description ="Principles and Practices/Final project sketch"
+++

I read the Fab Charter, it was interesting and I like the most that they are available for almost anyone!

### What will I do in the final project, how and why.

I'm not completely sure about this idea, but didn't come up with anything else yet so lets go with this for the start.
I got the idea for this project last summer, as my mother likes to dry their laundry outside in the summer. She was complaining to me that it's so annoying if you forget them there and it rains they will get all wet and take so long to dry again.
So this project could be an answer to that problem. The idea is to build an automatic sunshade/umbrella thingy that senses when raining(maybe even sunlight) and opens or closes accordingly.

## The automatic sunshade/umbrella

![Downloading the previous version from github](../../media/first_sketch_compressed.jpg)

You could also change to different modes or open and close it manually with buttons/remote/app.
Inside it would be a microcontroller, maybe a small arduino for offline project or then esp32 if network connectivity wouldn't be too big of a job to make. Connected to the microcontroller there would be a rechargeable battery and a stepper motor that will open and close the sunshade using pulleys and strings/rope, it could for example pull one larger rope which is connected to other strings.
There could also be a small solar panel on top to charge the battery automatically and to detect if it's sunny.

Here is the final project page:

https://onni-innovations.gitlab.io/fab-academy/final/
