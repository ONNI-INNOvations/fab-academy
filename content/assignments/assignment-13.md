+++
title = "My Thirteenth assignment"
description ="Output Devices"
+++

For the output devices week I wanted to design a board with an ATtiny412 that would control leds from an outside power source with the help of a mosfet.
I decided to use kicad for this.
First I started by creating the schematic and added all the components on it.

![LED-01](../../media/output/led_01.png)

Then I continued by connecting all of them together.

![LED-02](../../media/output/led_02.png)

Then I finished it by uploading the netlist to circuit designer, put on all the tracks and a ground fill.

![LED-03](../../media/output/led_03.png)

Next I designed the rml files for the router on mods.cda.mitd.edu

![LED-04](../../media/output/led_04.png)

Then I soldered this board and tried to get it working, but I managed to have some problems with the soldering and the board.

![LED-06](../../media/output/led.jpg)

I decided to modify it a little bit by moving the tracks a little and adding a power led with a resistor to see if the board is getting power.

![LED-05](../../media/output/led_05.png)

Next I cut it out, and soldered it.
The idea was to connect this board to the electric scateboard in order to have led strips under the deck that change their brightness according to a changeable variable. That's why I left also some of the extra pins connected to a header for easy connection to external boards.

I used this code to test the boards functionality with a 9 volt power supply to drive the board and led strip, this code makes the led brightness increase to max and then goes down to off completely with regular cycles:

```
#define max_threshold 100
#define max_cycle 10000
#define num_cycles 5
#define LED_pin PIN6_bm
#define LED_port VPORTA.OUT
#define set(port,pin) (port |= pin)
#define clr(port,pin) (port &= (~pin))

void setup() {
   PORTA.DIRSET = LED_pin;
   }

uint16_t cycle,cycles,threshold;

void loop() {
   while(1) {
      for (threshold = 0; threshold < max_threshold; ++threshold) {
         for (cycles = 0; cycles < num_cycles; ++cycles) {
            for (cycle = 0; cycle < threshold; ++cycle)
               set(LED_port,LED_pin);
            for (cycle = threshold; cycle < max_cycle; ++cycle)
               clr(LED_port,LED_pin);
            }
         }
      for (threshold = max_threshold; threshold > 0; --threshold) {
         for (cycles = 0; cycles < num_cycles; ++cycles) {
            for (cycle = 0; cycle < threshold; ++cycle)
               set(LED_port,LED_pin);
            for (cycle = threshold; cycle < max_cycle; ++cycle)
               clr(LED_port,LED_pin);
            }
         }
      }
   }
```

Then I uploaded this code using pyserial and a ftdi adapter and connection.
This is how it looks like working!:

{{< rawhtml >}}
<video width="320" height="240" controls>
  <source src="../../media/output/led.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
{{< /rawhtml >}}

Later on I still want to connect it to the scateboard and have it work together with it to have a cool led strip under the deck for darker nights.

Here's the  design files

{{< rawhtml >}}
  <p class="speshal-fancy-custom">
  <a href="../../files/LedBoard.zip" download >Ledboard design files</a>
  </p>
{{< /rawhtml >}}

(compressed the images wrongly, that's why they look a little fucked :D)
