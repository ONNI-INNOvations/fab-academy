+++
title = "My third assignment"
description ="Computer-Aided Design"
+++

Here I will be using Fusion 360, gravitysketch and Inkscape to produce 3d models and 2d sketches for my final project

## Fusion 360

I wanted to learn some more fusion 360 so I downloaded it using a student licence and decided to create a part for my final project. I wanted to make some example buttons for the final project. So first I open fusion and created a new project called Fablab final project.
Next I click to show the origin and right click a plane on the origin to start making a sketch. I make a rectangular shape and extrude it with thickness 2mm

![fusion-01](../../media/fusion-01.png)

Next I create an smaller rectangle inside this rectangle and extrude the sides.

![fusion-02](../../media/fusion-02.png)

Next I used the fillet tool to fillet the sides and make the button more rounded.

![fusion-03](../../media/fusion-03.png)

Next I select the top face again and choose create sketch. Then use the text tool to put text on it, next I extrude the text on the object
Here's the final model for the open button.

![fusion-04](../../media/fusion-04.png)

## Gravity Sketch
So I recently bought a vr headset and I wanted to 3d model with it. I downloaded the now free software Gravity Sketch and started modeling. My idea was to model a very basic lifesize prototype of the shade thingy. This was my first time modeling with this so I learned as I went.

Firstly I opened the menu and chose the revolve tool to make the shaft for the umbrella

![gs-1](../../media/gs-1.jpg)

Next I used the same tool to add the shade.

![gs-3](../../media/gs-3.jpg)

Next I used the shape tool and put some rectangles into it to act as buttons.

![gs-4](../../media/gs-4.jpg)

I didn't have a specific model for the rain catcher/water sensor so I made a crude example of how it might look.

![gs-5](../../media/gs-5.jpg)

Lastly I added some of the internal components inside the umbrella to see how they would fit and connect to each other.

![gs-6](../../media/gs-6.jpg)

And this is the final result!

![gs-2](../../media/gs-2.jpg)

Overall I think I will be using this tool a lot in the future to make easy and simple prototypes as it's really fast, easy to use and fun!

## Inkscape

I wanted to learn some 2D vectoring and Inkscape seemed like the tool I wanted to use for that!

Firstly I just wanted to try the basic tools out and made a simple rectangle with a fixed aspect ratio.

![Inkscape-01](../../media/inkscape-01.png)

After I had played around with that for a bit, I decided that I want to do something "cooler". I decided to use one of my pictures from the website as a base and make a svg that could for example be laser engraved on some material.
So I chose file and import from the left upper corner and opener my file.

![Inkscape-02](../../media/inkscape-02.png)

Next up I wanted to trace the bitmap of the picture to make it into an svg, I chose 8 scans and different colours so I could choose the best looking later.

![Inkscape-03](../../media/inkscape-03.png)

Here's the result of that tracing.

![Inkscape-04](../../media/inkscape-04.png)

Next I clicked object and ungroup in the upper left corner to separate all the different layers. I separated them and they looked like this with the reference pic on the centre.

![Inkscape-05](../../media/inkscape-05.png)

Here's the final layer I chose.

![Inkscape-06](../../media/inkscape-06.png)

Overall I think Inkscape is a really good free tool that can be really handy in the future, I like that it can make gcodes straight in the app, and the app was really fast to learn. I will surely use this with laser cutting in the future.

## Here are all the files that I made during this exercise.

{{< rawhtml >}}
  <p class="speshal-fancy-custom">
  <a href="../../files/files.zip" download >3d Files</a>
  </p>
{{< /rawhtml >}}
