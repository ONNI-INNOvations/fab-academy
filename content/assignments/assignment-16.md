+++
title = "My sixteenth assignment"
description ="Wildcard week"
+++

For this week I wanted to use the vacuum forming machine to create an extra protective layer for a electronics protection box I was going to make for the final project.
My idea at first was to make this electronics box with 3D printing so I got to designing it with fusion 360.
I didn't remember to document every step of the way but here's the lid, it was rdesigned so it would fit snugly inside the box with a tight fit, it also had screw holes for extra protection.

![W-01](../../media/vc/electronics.jpg)

The box was designed with holes for the motor wires and charger plug.

![W-02](../../media/vc/electronics2.jpg)

Sadly after 20 hours of printing the ultimaker decided to clog on me and I needed to start from scratch.
This time I wanted to design a laser cuttable box for faster making and troubleshooting.
This was how it looked in the end, pretty much just a laser cuttable version of the first one.

![W-03](../../media/vc/electronics3.jpg)

After confirming that all the components fit well in to the box and everything was within the right tolerances it was time to vacuum form.
I started by cutting a 2mm vacuum sheet into a right sized sheet with the laser cutter.

![W-04](../../media/vc/vacuum-03.jpg)

After that was done I put the box in the machine, loaded the acrylic sheet. Tightened everything up. Then I opened the connection for the hydraulic power, powered on the machine and put on the compressor.

![W-05](../../media/vc/vacuum-02.jpg)

After those were done I moved the heating element on top of the sheet and let it be there for around 100 seconds for the acrylic to get soft enough. During this time I used the blow up button inflating it into a kind of bubble. After it looked good, I took of the heating and started raising the bed. When the bed was raised I put the vacuum on to suck the extra air out to create the shape. Now I used the blow up button and vacuum a few more times to get the acrylic as close to the box as possible and it seemed to work out good enough

![W-06](../../media/vc/vacuum-01.jpg)

Now it was ready to let cool down and after that the table was lowered and the acrylic and box removed. I didn't have time yet but I'll add later the shaped acrylic on top of the electronics box on the bottom to add some extra protection for my final project.


The files for this project can be found here:

{{< rawhtml >}}
  <p class="speshal-fancy-custom">
  <a href="../../files/elecbox.zip" download >electronics box design files</a>
  </p>
{{< /rawhtml >}}
