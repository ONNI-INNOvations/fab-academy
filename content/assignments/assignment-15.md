+++
title = "My fifteenth assignment"
description ="Interface and Application Programming"
+++

This week I wanted to make an interface, show and visualise the output of my led output board, an adriardino and a light sensor.
Pic of adriardino:

![IAP-01](../../media/interface/if-04.jpg)

First I started out by testing a potentiometer script with the light sensor. I added a light sensor to the adriardino including the power and communication wire.
Next  I setup the right pins and put the it to work.

![IAP-02](../../media/interface/interface_02.png)

After that worked pretty easily I wanted to check what kind of data you can get out of the light sensor connected to the arduino copy.
I started by opening anaconda console, python3 on it and importing serial

![IAP-03](../../media/interface/interface_01.png)

Next I setup the serial monitor with port 12 and 9600 baud rate.
After that I read a line on it and see that the data format is rawly b'978\r\n' and I needed to get just 978 out of it.
I used the commands decode, strip and converted it to int. These can all be used in one line as well, but for testing it I used them one by one.

![IAP-04](../../media/interface/interface_03.png)

Next I wrote a simple python script that would write 50 readings.

![IAP-05](../../media/interface/interface_04.png)

After confirming that it worked I wrote a more complicated code that would read the reading of the sensor and show a graphical representation of it. In this I wanted the data being shown as a kind of progressbar.

![IAP-06](../../media/interface/if-02.jpg)

How the led output board was used together with it

![IAP-07](../../media/interface/if-03.jpg)


I also wanted to experiment with different kinds of data visualisations and decided to use an intresting library matplot to draw a graph of the recorded data form the light sensor.
This is the result:

![IAP-08](../../media/interface/if-05.jpg)


The files for this project can be found here:

{{< rawhtml >}}
  <p class="speshal-fancy-custom">
  <a href="../../files/interface.zip" download >interface design files</a>
  </p>
{{< /rawhtml >}}
