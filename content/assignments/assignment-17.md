+++
title = "My seventeenth assignment"
description ="Applications and Implications"
+++

I'm not completely sure if I understood this assignment completely right but lets hope so, can fix this after the summer if needed!

The ultimate electric longboard:

The idea would be to build an electric longboard to move easily and and affordably across urban terrain.
It would use premade design of the vesc with small modifications and the pcbs should be custom ordered in large quantities from chinse factories to get cheap and big quantities.
 Would need to decide how it would be to get the best and easiest results on this.
The wood used would be vinyl sheets that would be pressed together, the vinyl sheets would be cut and made in finland beforehand.
The board would use ready made motors and samsung 30q batteries made in china.
 Will need to find out if there are better options.
I will design individual board shapes and designs and cut them out of the vinyl using cnc machines.
I will also design trucks and wheels to use with the board, both of them would be casted.
Trucks out of aluminium and wheels out of rubber.
There would also be ball bearings used that would be ordered from china in big quantities.
The electronics box would be formed out of carbon fiber on top of a 3d printed mold
