+++
title = "My eleventh assignment"
description ="Input devices"
+++

For the input devices week I wanted to make a simple input device. I wasn't completely sure what this all needed and I saw it was possible to do with a button. So I got to designing a board which would use an attiny412 chip and a button to do something when the button is pressed.

So first I started designing the board. I opened kicad and added all the parts I would want in the board.

![in-1](../../media/inputdevices/input-01.png)

Next I labelled them and connected them as I wanted them and tested for electrical rules.

![in-3](../../media/inputdevices/input-03.png)

Then I opened a pcb editor in kicad and imported the previously made netlist.

![in-4](../../media/inputdevices/input-04.png)

Next I connected them as they should be connected

![in-5](../../media/inputdevices/input-05.png)

Then I created a no fill zone around the microchip and filled the extra area with front copper to connect the grounds.

![in-6](../../media/inputdevices/input-06.png)

Next Up I got it ready for milling and milled it

![in-7](../../media/inputdevices/input-08.png)

Then I soldered everything together and now the input device was ready.

![in-8](../../media/inputdevices/input-09.jpg)

Then I got to writing the code for the input devices assignment. I wanted to be able to press the button and then to get an answer to the serial monitor. I wanted to make the code as compact as possible

```
define button 3 // where the button is connected to

void setup() {
   Serial.begin(9600); // defines the baudrate
   pinMode(button,INPUT_PULLUP); // defines the button as input
   }
void loop() {

   while (digitalRead(button) != LOW) // Writes when the button is pressed
      ;
   Serial.write("pee");

   while (digitalRead(button) == LOW) // writes when the button is not pressed/released
      ;
   Serial.write("poo");
   }
````
I uploaded the code using updi and now the board was working!

Here's a video of it :
{{< rawhtml >}}
<video width="320" height="240" controls>
  <source src="../../media/inputdevices/input.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
{{< /rawhtml >}}


Here's the  design files

{{< rawhtml >}}
  <p class="speshal-fancy-custom">
  <a href="../../files/input devices.zip" download >Input device design files</a>
  </p>
{{< /rawhtml >}}
