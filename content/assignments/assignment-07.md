+++
title = "My seventh assignment"
description ="Electronics Design"
+++

This week was interesting as I learned how to use kicad and about the different attributes of designing different parts.

## Electronics design group assignment

Here you can find our groups design assignment: https://russian_wu.gitlab.io/fab-academy-2021/assignments/week07-electronics-design/week07-electronics-design/

## Electronics design

So I started just by watching the video and then decided to try to make my own version of the ATtiny412 echo board. Firstly I added downloaded the fab kicad library and added them into the symbol and footprint libraries.

![ELE-01](../../media/pcb/pcb-01.png)

Then I loaded the wanted components in to the project from the fab library.

![ELE-02](../../media/pcb/pcb-02.png)

Here are the basic components of the components for my first version.

![ELE-03](../../media/pcb/pcb-03.png)

Next I added connected all the parts together, labeled them and checked for electrical errors. After everything was good I created a netlist of the components and opened pcb layout editor imported the netlist and this is how it looked.

![ELE-04](../../media/pcb/pcb-04.png)

Then I moved them to other places to try to get it to work. There were quite a few problems with this and I didn't even realise at this point that the ftdi port was the wrong way around.

![ELE-05](../../media/pcb/pcb-05.png)

So I didn't like how that looked and some stuff was wrong so I flipped the ftdi and re-designed the project. Sadly now when I made this I didn't remember to change the track width.
So next I filled the rest of the board with front copper for it to act as ground. I also created no fill area around the ATtiny412 chip.

![ELE-06](../../media/pcb/pcb-06.png)

So In the previous design I didn't remember to change the track width so I designed to make the whole board again, I also wanted to connect the leds better and to the right pins so I fixed all of that.

![ELE-08](../../media/pcb/pcb-08.png)

Next I added the margin, copper fill as ground and no fill area around the chip.

![ELE-09](../../media/pcb/pcb-09.png)

Next I wanted to cut the board and these are the settings I used. This one I used with milling the outline.

![ELE-10](../../media/pcb/pcb-10.png)

These settings I used on cutting the traces(shows old version of board on computer because I didn't remember to take pic of latest version)

![ELE-11](../../media/pcb/pcb-11.jpg)

So next I soldered the components and soldered them like in electronics production week. Then I used the blink_412 code on arduino Ide to test the chip.

![ELE-12](../../media/pcb/pcb-12.jpg)

here is the soldered and working board. The blink example works for the led. I realised at this point why I had problems putting the other led on but It was because I got confused when soldering and thought I had designed the board wrong, started fixing with an utility knife and later realised that I had designed it right and was putting the component on a wrong rotation. Nevertheless I learned a lot about this project and the board is working and blinking.

![ELE-13](../../media/pcb/pcb-13.jpg)

This is how it looks, some led and resistor problems but they work now.

![ELE-14](../../media/pcb/pcb-14.jpg)

Here are all the pcb kicad files I got.

{{< rawhtml >}}
  <p class="speshal-fancy-custom">
  <a href="../../files/Project pcb.zip" download >pcb design</a>
  </p>
{{< /rawhtml >}}
