+++
title = "My first assignment"
description ="Project Management"
+++

My first assignment was to build a website using html5 and Hugo.
I did most of the work at the same time during the zoom, but I polished and finished it in the evenings after the zoom meetings.

## Setup
I downloaded the recommended softwares for windows for this, and I have liked them. They are called Atom and Cmder
I'm using a dual monitor setup, with an extra monitor connected to my laptop. I usually run a video/zoom/website/instructions on the upper screen and Cmder and atom on the other one.

![Downloading the previous version from github](../../media/Setup_compressed.jpg)

At first I created a website that worked locally, and I checked on it using Firefox, with developer settings to see if everything works. At the moment, the pictures are too big on mobile, but I will fix this later.

## Git

Next I created a gitlab account online and studied its basics. I generated a new ssh key in my computer and added it to Gitlab.
```
ssh-keygen -t ed25519 -C "eriksson.onni@gmail.com"
```
Now Gitlab was working and I could add more stuff to it realtime. I could use the following commands to check the status, update the files, add commits and upload them to git.

```
git status (Shows status)

git add . (You can replace the dot with the file you want to add to the git, dot adds all files)

git commit -m "what you want to say" (Commits the added files with your comment)

git push (Pushes all the files to gitlab)

```

After I had put my first files In gitlab, I added a new file called .gitlab-ci.yml straight from online gitlab and chose the ready hugo template.
Then I used a command to pull the files to my own computer and sync them
```
git pull
```

Mostly I just used these same commands with different variables in git. I think it's really handy and a great tool which I will continue to use later!

## Hugo
I downloaded hugo online and extracted it to a folder in its own folder in C:\Hugo.
I'm using windows 10 so I needed to add hugo to the path for it to run easily from anywhere in Cmder, for some reason I have to do this again everytime I launch Cmder to use hugo.

```
PATH=%PATH%;C:\Hugo
```
After that I navigate to the folder my website is located in and use cd to navigate to the website folder, then I use launch it locally with hugo.

```
cd C:\Users\Onni\Desktop\FABLAB\website

hugo server
```

## Bootstrap

I tried following last Thursday lectures about bootstrap and css, but stupidly I tried to do the things at the same time as the video. Well eventually I bricked my server and don't even remember what I did, I will now fall back to the last save I put up in github.

![Downloading the previous version from github](../../media/redownloadgithub_compressed.jpg)

This week was really busy because of my other deadlines, so I didn't have time to put the bootstrap back on and put css there, I will do that later.

## Image optimizing

I needed to optimize my images as they were taking a long time to load, and I had no need for extremely sharp pictures, I tried some different quality settings and resolutions for the images to see which has the best speed/quality.
I downloaded a tool called caesium for this in windows and it has worked well.
I optimized the pictures and replaced them with the originals, this image of the image optimization tool is really compressed, that's why the quality isn't the best.

![Image Optimization using caesium](../../media/caesium_compressed.jpg)

I used the optimized images mostly in this assignment section so far, but I will keep using it later on also!

## Final Words

It was quite a lot of work and I'm still not quite done with the website, but overall it was a really great and interesting assignment where I learned a lot!
I had to build the website from the ground up/from the last save a few times because of bugs or I didn't just like how it looked. Now I'm back to the very basic form of the website. Simply the best right? I will update the website later and add css for example. I will also fix the pictures so they will look better on mobile.
It was great that I learned how to write html5 and to use Gitlab, I have always been interested in these but have never actually taken the time to learn them.
