+++
title = "My twelth assignment"
description ="Molding and Casting"
+++

For the molding and casting week I wanted to make a scateboard deck.

So I started designing the mold with fusion 360.
First I designed the how it would look from up. Just drew an sketch with some changeable variables for ease of changing later. Then I extruded it.

![deck-1](../../media/MD/MD-01.png)

Next I wanted to add the curve for it because I wanted the center to be below the sides for it to look cool

![deck-2](../../media/MD/MD-02.png)

Then I used intersect to get the right shapes put together to get the shape of my deck.

![deck-4](../../media/MD/MD-04.png)

Next I added some blocks that I would cut from the wood. These would be the vertical blocks.

![deck-5](../../media/MD/MD-05.png)

Then I added horizontal blocks and slots in them for the vertical blocks.
Lastly I cut all the blocks with the scateboard shape to get the right shape. I decided at this point that It will be easier to first make the basic scateboard and then cut it to shape.

![deck-6](../../media/MD/MD-06.png)

Next up when that was ready I was ready to assemble the mold.

![deck-8](../../media/MD/MD-08.jpg)

When it was assembled I gathered 8 vinyl sheets and placed them one by one on the mold while applying a thin coating of glue between every layer with the help of a spatula.

![deck-9](../../media/MD/MD-09.jpg)

Then I clamped it dwon with multiple clamps and left it to harden and dry.

Here's how the final result looked:

![deck-10](../../media/MD/MD-10.jpg)

 In hindsight I didn't add enough clamps and also the shape of the board could have been cut beforehand. I will be doing this again for the final project but nevertheless this was a great learning experience and I think I learned a lot more about molding and casting.

Here's the  design files

{{< rawhtml >}}
  <p class="speshal-fancy-custom">
  <a href="../../files/scateboarddeck.zip" download >Scateboard deck mold design files and dxf files</a>
  </p>
{{< /rawhtml >}}
