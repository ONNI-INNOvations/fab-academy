+++
title = "Final Project"
description= "This is my Final-project documentation site"
+++

For my final project I wanted to build an electric longboard. I had a different idea at first, but then when I gave it more thought I realised this is what I wanted to build and I have motivation and interest to finish this project!
I also thought about a scooter, but it seemed too easy and they are quite common nowdays, the longboard should be more fun to ride as well.

I started the project by watching a lot of youtube videos about people building their own boards and looking at different forums for example : https://electric-skateboard.builders/. This was a very useful and goof forum to get information and help in. It seemed like a cool project so I gathered a parts list and ordered some of the parts myself and some through my local teacher.
In the meanwhile I remembered my parent's house had a old scateboard and I thought there might be something in it that I could use.
### The deck

First thing that I noticed that the trucks and wheels were in ok shape but a little rusty. So I got to cleaning them up with wd-40, paper and water.

![ol-01](../media/final/oldboard-01.jpg)

When I got the trucks cleaned up I removed them.
That's when I noticed that the screws and ball bearings in the wheels that were in use were damaged beyond repair by rust, but gladly all the important parts were in pretty good shape.
There were fitting m5 bolts and nuts and the right sized ball bearings already in the lab so I didn't need to order more.

![ol-02](../media/final/oldboard-02.jpg)

While still waiting for the parts to arrive I wanted to start making the actual deck.
At first my idea was to make a more of a shaped scateboard deck than a longboard deck.
I designed the mold, cnc cut it and glued some plywood sheets together and put them in the mold overnight.
More details about this in my week 12 documentation of molding and casting.

The overall result didn't work out as well as I wanted and during this tome after some more research I came to the conclusion that with my non existent skill level of riding a longboard a simpler deck is probably better.
So next I started designing a straight deck.
Firstly I decided to glue the plywood sheets together and press them between two big wood pieces with a lot of clamps to get a straight deck base. I used 2 more stiff sheets in the center, then 2 on the both sizes and finally very thin better looking sheet as the top.

![br-01](../media/final/board-01.jpg)

For the deck shape I used the template from the previous design in the molding and casting week.

![br-02](../media/final/cnc-1.png)

First I cut it out of vinyl and put it on top of the glued plywood base, but after my local instructor recommended me to use a cnc rather than cutting it by hand I realised that yeah it's probably a lot better way to go. So I loaded the base into the cnc machine and got the settings really well tuned in as well as very proper vacuum. I also made the cnc to drill the mounting holes for the trucks at the same time.

![br-03](../media/final/board-03.jpg)

It came out looking pretty good from the cnc. But I wanted to still give it a nicer finish so I first sanded it with a sanding machine, wetted it, sanded it again and then applied cotton oil to it to give it a nicer look and some weather resistance.
This is how the deck ended up looking, I'm pretty happy with the result personally.

![br-04](../media/final/board-04.jpg)

### The escs and motors.

For the esc I wanted to use open source vesc because it looked like the best variant with all of it's flexibility.
https://vesc-project.com/ I found flipskys manufactured versions pretty cheaply so I decided to go with them.
For the motors I decided to go with 190kv 6384 motors for their big power and torque. They would have no problem getting me to speed.
With the motors I was also using a chain and gears to drive the wheels to get a 14/27 ratio and to have some extra torque and speed.

Firstly I needed to add the big gears to the wheels. I cleaned the wheels and placed new bearings in them.  Then I started by placing bearings inside the big gear and then aligned them with a bolt just the right size.

![e-01](../media/final/gear-01.jpg)

Then when it was aligned I drilled four holes to the wheels with a drill and installed the big gear with screws to the wheel.

![e-02](../media/final/gear-02.jpg)

Next I started working on the escs. I started by soldering the power plugs to the boards.

![e-03](../media/final/esc-01.jpg)

After that I had a problem about connecting the motor wires to the escs but luckily I noticed that the xt60 connectors were just exactly the right size.

![e-04](../media/final/esc-02.jpg)

So I cut them with an iron saw in two, cleaned them up with pliers and started soldering them to the motor wires in the escs

![e-05](../media/final/esc-05.jpg)

![e-06](../media/final/esc-06.jpg)

Now the esc connections were ready so I downloaded the vesc tool :https://vesc-project.com/vesc_tool for esc programming.

![e-07](../media/final/esc-07.jpg)

Next up I connected the vesc to the computer with an included micro usb cable, the motor wires to a motor and the power connectors to an extra 6s lipo battery I had.

![e-08](../media/final/esc-08.jpg)

Firstly I decided to calibrate the motor as a test, I added the asked variables, it spun it up and it worked.

![e-09](../media/final/vesc.png)


#### MY NEMESIS

Next I started working on motorholders to attach to the truck, they were easily the most frustrating and time taking thing in this whole project even though it seemed like an easy thing in the start. I really probably made more than 20 different designs and versions, there's so many that I don't think there is any use of me explaining them, but the basic idea was the same thing all the time. To get the big and powerful motor attached to the truck firmly in a certain changeable distance.
I will explain the first one and the chain a little better.
This is how I thought it would be attached in the started

![f-01](../media/final/wheel-04.jpg)

I also noticed that the motor gears I ordered were 8mm and my motor shaft was 10mm, so I drilled the whole bigger with a big drill press.

![f-02](../media/final/wheel-06.jpg)

This is me testing the design with the chain installed, this is how it would work.

![f-03](../media/final/wheel-07.jpg)

it didn't really work so well so I printed a small piece under it and tested it as well.

![f-04](../media/final/wheel-08.jpg)

Here are some designs I made from the start to the final one.

![m-01](../media/final/waterjet_01.png)
![m-02](../media/final/waterjet_02.png)
![m-03](../media/final/waterjet_03.png)
![m-04](../media/final/waterjet_04.png)
![m-05](../media/final/waterjet_05.png)
![m-06](../media/final/waterjet_06.png)

Here is how the designs evolved as well and moving slowly from just laser cutting to also 3D-printing.

![m-07](../media/final/holder-01.jpg)
![m-08](../media/final/holder-02.jpg)
![m-09](../media/final/holder-03.jpg)
![m-10](../media/final/holder-04.jpg)

### Wheels

The current wheels were only 60mm diameter and I wanted around 80mm wheels and after googling quite a lot I found it was possible to 3d print them with tpu and get pretty good results.
So I got to designing my own wheel design.
I made it using fusion 360 and changeable parameters to chagne them later on if needed. I also wanted to add some grooves with the circular pattern function in fusion.

![w-01](../media/final/wheel-01.png)
![w-02](../media/final/wheel-02.png)
![w-03](../media/final/wheel-03.png)

I wanted to print these out of tpu but there was only 1.75mm ninjaflex in the lab and no 1.75mm machine to use them on. So I decided to print the wheels at home with my prusa mk3s.
I used 15% 3d honeycomb infill and no top and bottom layers with 3 perimeters on a 0.6mm nozzle to get a hard but a little giving wheel with a good look.

![w-04](../media/final/wheelie.jpg)

These worked well and snapped tightly straight on top of the current wheels.

### The batteries.

For the batteries I decided to use samsung 18650 30q battery cells because they have a high constant output of 15-20A and 3000mAh size per cell.
I decided to use 18 of them to build a 6s3p battery pack to get decent speed with long range.

Firstly I modified the existing 3 cell battery holders with custom nickel strip connections because I didn't trust that the ready ones could have handled a lot of amps.
This is how 3 batteries looked in the modified cell with nickel connections. The idea is to have 6 of these 3 packs so in the 3 packs the batteries are in paraller and then these are packs are wired in series.

![b-01](../media/final/btr-01.jpg)

All of the packs done

![b-02](../media/final/btr-02.jpg)

Next I soldered the packs together in series and super glued the packs accordingly together to not have them loose and to create some extra rigidity.

![b-03](../media/final/btr-03.jpg)

Then I wanted to add my custom bms with balance cell feature for charging the batteries and keeping the cells voltages balanced between each other to not damage the batteris while charging.
I added all the necessary wires from the bms between all the cells and the ends. I also soldered a charging end for my smart charger in the end.

![b-04](../media/final/btr-04.jpg)

I needed to make also a case for all the electronics and batteries to protect them. First I designed them to be 3d printed but the print failed halfway through so I decided to just laser cut it to get it faster and get a nicer look.
This is how the design looked in fusion 360

![b-05](../media/final/electronicsbox3.png)

I decided it need some extra protection so I used 2mm acrylic and vacuum forming machine to make an extra protective layer for the electronics that can be screwed on top of the wooden one. More info on this can be or will be found at wildcard week assingment page!

![b-06](../media/final/vacuum-01.jpg)

I also designed a custom pcb board to control leds with a mosfet on the board. I wanted to add leds on the bottom that will change brightness according to the outside light. I made the board and got it working, I didn't have time to yet add the light sensor on it but will add it in the future.

![b-07](../media/final/output-01.png)

### Coming together

Finally all of this comes together.
I glued the electronics boxes corners to get it watertight.
I attached the escs to the electronics box

![f-01](../media/final/final-01.jpg)

Then I added all the rest of the stuff inside and connected the wires.
I still need to add switches for the power so I didn't put the upper lid properly on, just with duct tape for now.

![f-02](../media/final/final-02.jpg)

Here's a video of me driving the deck by itself outside and testing it.

{{< rawhtml >}}
<video width="320" height="240" controls>
  <source src="../media/final/final-01.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
{{< /rawhtml >}}

And also a video of Daniel riding the longboard, it works great! Only thing is that the motor mounts need to be made better and more stable and I'd like to fix it in the future but I already have some ideas for that!

{{< rawhtml >}}
<video width="320" height="240" controls>
  <source src="../media/final/final-03.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
{{< /rawhtml >}}

Here's all the design files related to the final project so far.

{{< rawhtml >}}
  <p class="speshal-fancy-custom">
  <a href="../files/finalproject.zip" download >Files</a>
  </p>
{{< /rawhtml >}}
